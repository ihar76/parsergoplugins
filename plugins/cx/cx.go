package main

import (
	"bitbucket.org/ihar76/parsergoplugins/types"
	"fmt"
	"encoding/json"
	"github.com/satori/go.uuid"
)

type first string

func (f first) GetCode() string {
	return "CX"
}


func main(){
	proxy :=types.Proxy{}
	uuID,_:=uuid.NewV1()
	query := []byte(fmt.Sprintf(`{
   "id": "%s",
   "parser": "CX",
   "type": "rt",
   "proxyService":"http://beta.flightguru.com:5050",
   "proxyType": "squid",
   "passengers": 1,
   "slices": [
       {
           "from":"SFO",
           "to":"HKG",
           "dates": ["2018-08-31"],
           "direction": 0
       }
   ],
   "cabins": ["E"]
	}`, uuID.String()))
	trips := Parser.Run(query, proxy)
	fmt.Println(trips)

}

func (f first) Run(str []byte, proxy types.Proxy) []types.Trip {

	fmt.Println(f.GetCode() + " parse")
	params := Params{}
	json.Unmarshal(str, &params)
	trips := []types.Trip{}
	for _, slice := range params.Slices {
		slice.Proxy = proxy
		slice.Passengers = params.Passengers
		trips = append(trips, slice.RunTravel(params.ID, params.Cabins)...)
	}

	return trips
}

func (t *Slice) RunTravel(ID string, cabins []string) []types.Trip {
	trips := []types.Trip{}

	for _, date := range t.Dates {
		trips = append(trips, t.SearchByDay(date, ID, cabins)...)
	}

	return trips
}

var Parser first

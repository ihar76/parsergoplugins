package main

import (
	"encoding/json"
	"bitbucket.org/ihar76/parsergoplugins/types"
	"github.com/satori/go.uuid"
	"os"
	//"time"
)

//func StartProcess(p ) {
//	for _, slice := range p.Slices {
//		slice.Passengers = p.Passengers
//		slice.RunTravel(p.ID)
//	}
//}

func (t * Slice)SearchByDay(day Date, ID string, cabins []string) []types.Trip{
	url := "https://smartphone.united.com/UnitedMobileDataServices/api/Shopping/Shop?UID=2.1.54A&DID=65743fdd38127922"
	data := GetRequestData{}
	UuidM,_ := uuid.NewV4()
	Uuid := UuidM.String()
	data.fillRequest(t, day, Uuid)
	dataStr, _ := json.Marshal(data)
	body := types.PostJSON(url, string(dataStr), Headers, t.Proxy)
	f, _ := os.Create("/home/ivan/Projects/parsers/ua-parser/cookies/3.json")
	f.WriteString(body)
	f.Close()
	res := ResponseMainData{}
	if body != "" {
		json.Unmarshal([]byte(body), &res)
	}

	url = "https://smartphone.united.com/UnitedMobileDataServices/api/Shopping/OrganizeShopResults?UID=2.1.53A"
	if res.Availability.Trip.TotalFlightCount > 15 {

		req2 := GetRequestDataSecond{}
		req2.fillRequestSecond(t, day, Uuid, res.Availability.SessionID, 1)
		dataStr, _ := json.Marshal(req2)
		body := types.PostJSON(url, string(dataStr), Headers, t.Proxy)

		res2 := ResponseMainData{}
		json.Unmarshal([]byte(body), &res2)

		res.Availability.Trip.FlattenedFlights = append(res.Availability.Trip.FlattenedFlights, res2.Availability.Trip.FlattenedFlights...)

	}

	return res.startParse(t.Direction, ID, cabins)

}

func (ff *FlattenFlight) insert(index int, flights []FlattenFlight) {
	flights[index] = *ff
}

func (r *GetRequestData) fillRequest(t *Slice, day Date, Uuid string) {
	UuidM,_ := uuid.NewV4()
	Uuid2 := UuidM.String()
	r.AccessCode = "ACCESSCODE"
	r.Application.fill()
	r.AwardTravel = true
	r.CountryCode = "US"
	r.DeviceID = Uuid
	r.FareType = "lf"
	r.GetNonStopFlightsOnly = false
	r.IsELFFareDisplayAtFSR = true
	r.LanguageCode = "en-US"
	r.LengthOfCalendar = 3
	r.MaxNumberOfStops = 2
	r.MaxNumberOfTrips = 250
	r.MileagePlusAccountNumber = "NY143299"
	r.NumberOfAdults = t.Passengers
	r.ResultSortType = "R"
	r.SearchType = "OW"
	r.TransactionID = Uuid + "|" + Uuid2
	r.Trips = append(r.Trips, Trip{
		Cabin:       "first",
		DepartDate:  day.Formatted("01/02/2006"),
		Destination: t.To,
		Origin:      t.From,
	})

}

func (r *GetRequestDataSecond) fillRequestSecond(t *Slice, day Date, Uuid, SessionId string, page int) {
	r.AccessCode = " "
	r.Application.fill()
	r.DeviceID = Uuid
	r.LanguageCode = "en-US"
	r.LastTripIndexRequested = page
	r.SearchFiltersIn.AirportsDestination = t.To
	r.SearchFiltersIn.AirportsOrigin = t.From
	r.SearchFiltersIn.CabinCountMax = 4
	r.SearchFiltersIn.CarrierDefault = true
	r.SearchFiltersIn.CarrierExpress = true
	r.SearchFiltersIn.CarrierStar = true
	r.SearchFiltersIn.DurationMax = 16700
	r.SearchFiltersIn.DurationMin = 43
	r.SearchFiltersIn.DurationMax = -1
	r.SearchFiltersIn.DurationMin = -1
	r.SearchFiltersIn.PageNumber = page + 1
	r.SearchFiltersIn.PriceMax = 50000000
	r.SearchFiltersIn.PriceMin = 1.0
	r.SearchFiltersIn.ShowDepartureFilters = true
	r.SearchFiltersIn.ShowDurationFilters = true
	r.SearchFiltersIn.ShowLayOverFilters = true
	r.SearchFiltersIn.ShowSortingandFilters = true
	r.SearchFiltersIn.StopCountExcl = -1
	r.SearchFiltersIn.StopCountMax = 3
	r.SearchFiltersIn.StopCountMin = 0
	r.SessionID = SessionId

}

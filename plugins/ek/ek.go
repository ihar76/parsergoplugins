package main

import (
	"bitbucket.org/ihar76/parsergoplugins/types"
	"fmt"
	"encoding/json"
	_ "github.com/joho/godotenv/autoload"
	"net/http/cookiejar"
	"golang.org/x/net/publicsuffix"
	"interfaceExample/classChild"
	"time"
)

type first string

const USER_AGENT = "Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393"


var (

	InParams  Params
	t1        time.Time
	trips     []classChild.Trip
)


func Handle(body []byte) {
	json.Unmarshal(body, &InParams)
	t1 = time.Now();
	fmt.Printf("Elapsed time: %v\n", time.Since(t1));
	t1 = time.Now()
	InParams.StartProcess(string(body))
}


func main() {
	classChild.ServeHTTP(Handle)
}

func (p Params) StartProcess(paramsStart string) {
	f := first("")
	proxies := types.GetProxies("EK")
	trips := f.Run([]byte(paramsStart), proxies.GetProxy())
	if trips == nil {

	}
}

func main_old() {
	params := []byte(`{
   "id": "23111-1111-1111-1111-111111111302",
   "parser": "EK",
   "type": "rt",
   "proxyService":"http://beta.flightguru.com:5050",
   "proxyType": "squid",
   "passengers": 1,
   "slices": [
       {
           "from":"SFO",
           "to":"HKG",
           "dates": ["2018-12-09"],
           "direction": 0
       }
   ],
   "cabins": ["E"]
}`)
	f := first("")
	//proxy := types.Proxy{
	//	""
	//}
	proxies := types.GetProxies("EK")
	trips := f.Run([]byte(params), proxies.GetProxy())
	if trips == nil {

	}


}

func (f first) GetCode() string {
	return "EK"
}

func (f first) Run(str []byte, proxy types.Proxy) []types.Trip {

	fmt.Println(f.GetCode() + " parse")
	params := Params{}
	json.Unmarshal(str, &params)
	trips := []types.Trip{}
	for _, slice := range params.Slices {
		slice.Proxy = proxy
		slice.Passengers = params.Passengers
		trips = append(trips, slice.RunTravel(params.ID, params.Cabins)...)
	}

	return trips
}

func (t *Slice) RunTravel(ID string, cabins []string) []types.Trip {
	trips := []types.Trip{}


	jar, _ := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	t.login(jar)


	if t.Jars == nil {
		t.Jars = map[string]cookiejar.Jar{}
	}

	for _, date := range t.Dates {
		dateStr := date.ToString()
		t.Jars[dateStr] = *jar
		trips = append(trips, t.SearchByDay(date, ID, cabins)...)
	}

	return trips
}

var Parser first

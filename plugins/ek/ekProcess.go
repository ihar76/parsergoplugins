package main

import (
	"encoding/json"
	"bitbucket.org/ihar76/parsergoplugins/types"
	//"time"
	"net/http/cookiejar"
	"os"
	"net/http"
	"time"
	"net/url"
	"strconv"
)


func (t *Slice) SearchByDay(day Date, ID string, cabins []string) []types.Trip {
	url := "https://smartphone.united.com/UnitedMobileDataServices/api/Shopping/Shop?UID=2.1.54A&DID=65743fdd38127922"
	data := GetRequestData{}
	//UuidM,_ := uuid.NewV4()
	//Uuid := UuidM.String()

	dataStr, _ := json.Marshal(data)
	jar := t.Jars[day.ToString()]
	body, _ := types.PostJSON(url, string(dataStr), Headers, t.Proxy, &jar)
	//f, _ := os.Create("/home/ivan/Projects/parsers/ua-parser/cookies/3.json")
	//f.WriteString(body)
	//f.Close()
	res := ResponseMainData{}
	if body != "" {
		json.Unmarshal([]byte(body), &res)
	}

	url = "https://smartphone.united.com/UnitedMobileDataServices/api/Shopping/OrganizeShopResults?UID=2.1.53A"
	if res.Availability.Trip.TotalFlightCount > 15 {

		//req2 := GetRequestDataSecond{}
		//req2.fillRequestSecond(t, day, Uuid, res.Availability.SessionID, 1)
		//dataStr, _ := json.Marshal(req2)
		body, _ := types.PostJSON(url, string(dataStr), Headers, t.Proxy, &jar)

		res2 := ResponseMainData{}
		json.Unmarshal([]byte(body), &res2)

		res.Availability.Trip.FlattenedFlights = append(res.Availability.Trip.FlattenedFlights, res2.Availability.Trip.FlattenedFlights...)

	}

	return res.startParse(t.Direction, ID, cabins)

}

func (t *Slice) login(jar *cookiejar.Jar) {


	t.GetAccount("EK", true)

	cookiesSl, _ := getCookiesInUrl("https://www.emirates.com", t.Slice)

	if cookiesSl != nil {
		urlCookie, _ := url.Parse("https://www.emirates.com")
		(*jar).SetCookies(urlCookie, cookiesSl)
	}



}

func (t *Slice) openSessionPage(src string, jar *cookiejar.Jar) {
	url := "https://www.emirates.com" + src
	types.Get(url, t.Proxy, jar, Headers)
}

func (t *Slice) sendSensorData2(jar *cookiejar.Jar) {
	url := "https://www.emirates.com/_bm/async.js"

	headers := Headers
	headers["ADRUM"] = "isAjax:true"
	headers["referer"] = "https://www.emirates.com/account/english/login/login.aspx"

	types.Get(url, t.Proxy, jar, Headers)

	sensorData := t.getSensorData(jar)
	if sensorData == "" {

	}

	headers["referer"] = "https://www.emirates.com/account/english/login/login.aspx"
	url = "https://www.emirates.com/_bm/_data"
	sData := stringSD{sensorData}
	sOutData, _ := json.Marshal(sData)
	resp, raw := types.PostJSON(url, string(sOutData), headers, t.Proxy, jar)

	if resp == "" && raw != nil {

	}

}

func (t *Slice) sendLoginRequest(viewState, viewStateGenerator string, jar *cookiejar.Jar) (string, *http.Response) {

	headers := Headers

	headers["referer"] = "https://www.emirates.com/account/us/english/login/login.aspx"
	headers["content-Type"] = "application/x-www-form-urlencoded"
	headers["accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	headers["user-agent"] = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"

	strconv.Itoa(int(types.MakeTimestamp()))
	paramsC := "s=" + strconv.Itoa(int(types.MakeTimestamp())) + "&r=" + headers["referer"] + "?0"
	urladdr := url.URL{}
	urladdr.UnmarshalBinary([]byte("https://www.emirates.com"))

	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := http.Cookie{Name: "ADRUM", Value: paramsC, Expires: expiration}
	arrCookies := []*http.Cookie{}
	arrCookies = append(arrCookies, &cookie)
	jar.SetCookies(&urladdr, arrCookies)


	//delete(headers,"ADRUM")

	params := LoginRequest{
		VIEWSTATE:                     viewState,
		VIEWSTATEGENERATOR:            viewStateGenerator,
		Ctl00HdnmainCaptcha:           2,
		Ctl00LiveChatHdnChatOnTrigger: true,
		Ctl00LoginControlHdnBot:       1,
		HdnForgotpwd:                  "/account/uk/english/login/forgot_membership_password.aspx",
		HdnIsMessageBarEnabled:        true,
		HdnLoginCaptchaMode:           true,
		HdnShowCookieBar:              true,
		Heflg:                         "e",
		TxtMembershipNo:               t.Account.Login,
		TxtPassword:                   t.Account.Password,
		BtnLoginLoginWidget:           "Log id"}

	structParams := types.StructToMap(&params)
	body, raw := types.PostForm("https://www.emirates.com/account/us/english/login/login.aspx", structParams, headers, t.Proxy, jar)

	if body == "" {

	}
	return body, raw

}


func (t *Slice) getSensorData(jar *cookiejar.Jar) string {
	sensorData := types.Get(os.Getenv("EK_SENSOR_DATA_ENDPOINT"), types.Proxy{}, jar, Headers);
	resSd := responseSD{}
	json.Unmarshal([]byte(sensorData), &resSd);
	stringsd := stringSD{}

	json.Unmarshal([]byte(resSd.Response), &stringsd);
	sensorData = stringsd.SensorData

	return sensorData;
}

func (ff *FlattenFlight) insert(index int, flights []FlattenFlight) {
	flights[index] = *ff
}


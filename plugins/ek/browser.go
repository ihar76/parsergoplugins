package main

import (
	"context"
	"flag"
	"log"
	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/chromedp"
	"os"
	"github.com/chromedp/cdproto/cdp"
	"time"
	"fmt"
	"bytes"
	"net/http"
	"bitbucket.org/ihar76/parsergoplugins/types"
	"github.com/PuerkitoBio/goquery"
	"github.com/chromedp/chromedp/runner"
	"strconv"
)

type ud struct {
	URL, Description string
}

type (
	OutCookie struct {
		Name       string    `json:"name"`
		Value      string    `json:"value"`
		Path       string    `json:"path"`
		Domain     string    `json:"domain"`
		Expires    time.Time `json:"expires1"`
		RawExpires string
		MaxAge     int       `json:"maxAge"`
		Secure     bool      `json:"secure"`
		HttpOnly   bool      `json:"httpOnly"`
		Raw        string
		Unparsed   []string
	}
	ResultCookie struct {
		Cookies []OutCookie `json:"cookies"`
	}
	AllCookies struct {
		ID     int          `json:"id"`
		Result ResultCookie `json:"result"`
	}
)

func getCookiesInUrl(urlStr string, prx types.Slice) ([]*http.Cookie, string) {
	var err error
	debug := true

	flag.Parse()

	proxy := "http://" + prx.Proxy.ProxyLogin + ":" + prx.Proxy.ProxyPassword + "@" + prx.Proxy.IP + ":" + strconv.Itoa(prx.Proxy.Port)
	os.Setenv("http_proxy", proxy)
	os.Setenv("https_proxy", proxy)

	// create context
	ctxt, cancel := context.WithCancel(context.Background())
	defer cancel()

	var b bytes.Buffer
	log.SetOutput(&b)

	// create chrome instance
	var c *chromedp.CDP
	if debug {
		c, _ = chromedp.New(ctxt, chromedp.WithLog(log.Printf))
	} else {

		c, err = chromedp.New(
			ctxt, chromedp.WithRunnerOptions(
				//runner.Flag("no-sandbox", true),
				//runner.ExecPath("/usr/bin/google-chrome"),
				runner.Flag("headless", true),
				runner.Flag("disable-gpu", true),
				runner.Flag("port", 9222),
			), chromedp.WithLog(log.Printf),
		)
		if err != nil {
			fmt.Println(err)
		}
	}

	//open main page
	err = listAwesomeGoProjects(ctxt, c, "Selenium and browser control tools.", urlStr)

	if err != nil {
		//log.Fatal(err)
	}

	// open login page
	var resLink string
	err = c.Run(ctxt, getLink(&resLink))
	if err != nil {
		//log.Fatal(err)
	}

	preLink := ""
	doc2, _ := goquery.NewDocumentFromReader(bytes.NewReader([]byte(resLink)))

	resDoc2 := doc2.Find(".automation-toolbar-login");
	preLink, _ = resDoc2.Last().Attr("href")

	resLinkHref := "https://www.emirates.com" + preLink
	c.Run(ctxt, openLoginPage(resLinkHref))

	// process login
	var projects []*network.Cookie
	c.Run(ctxt, prLogin(prx.Account.Login, prx.Account.Password, &projects))

	// shutdown chrome
	err = c.Shutdown(ctxt)
	if err != nil {
		log.Fatal(err)
	}

	// wait for chrome to finish
	err = c.Wait()
	if err != nil {
		log.Fatal(err)
	}

	cookiesAllResult := [] *http.Cookie{}

	// convert cookies
	for _, cook := range projects {
		k := http.Cookie{
			cook.Name,
			cook.Value,
			cook.Path,
			cook.Domain,
			time.Time{},
			"",
			0,
			cook.Secure,
			cook.HTTPOnly,
			"",
			[]string{""},
		}

		cookiesAllResult = append(cookiesAllResult, &k)
	}

	return cookiesAllResult, resLink

}

func setheaders(host string, headers map[string]interface{}, res *string) chromedp.Tasks {
	return chromedp.Tasks{
		network.SetExtraHTTPHeaders(network.Headers(headers)),
		chromedp.Navigate(host),
		chromedp.Sleep(1 * time.Second),
		chromedp.Text(`#result`, res, chromedp.ByID, chromedp.NodeVisible),
	}
}

func getCookies(res *string) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.ActionFunc(func(ctxt context.Context, h cdp.Executor) error {
			_, err := network.GetAllCookies().Do(ctxt, h)
			if err != nil {
				fmt.Println(err)
				return err
			}
			return nil
		}),
	}
}

//specific get context for antibot security
func listAwesomeGoProjects(ctxt context.Context, c *chromedp.CDP, sect, urlStr string) error {
	// force max timeout of 15 seconds for retrieving and processing the data
	var cancel func()
	//ctxt, cancel = context.WithCancel(ctxt)
	ctxt, cancel = context.WithTimeout(ctxt, 5*time.Second)
	defer cancel()

	var res1 string
	if err := c.Run(ctxt, setheaders(
		urlStr,
		map[string]interface{}{
			"DNT": "1",
		}, &res1)); err != nil {
	}

	return nil
}

func getLink(res *string) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.OuterHTML(`.toolbar__link--login`, res, chromedp.NodeVisible, chromedp.ByQuery),
	}
}

func openLoginPage(loginPageUrl string) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(loginPageUrl),
		chromedp.WaitVisible(`#txtMembershipNo`, chromedp.ByID),
	}
}

func prLogin(qName, qPass string, pr *[]*network.Cookie) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.SendKeys(`#txtMembershipNo`, qName, chromedp.ByID),
		chromedp.SendKeys(`#txtPassword`, qPass+"\n", chromedp.ByID),
		chromedp.WaitVisible(`.main-navigation__link-toolbar-text--user-name`, chromedp.ByQuery),
		chromedp.ActionFunc(func(ctxt context.Context, h cdp.Executor) error {
			*pr, _ = network.GetAllCookies().Do(ctxt, h)
			return nil
		}),
	}
}

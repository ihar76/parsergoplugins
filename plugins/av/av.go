package main

import (
	types "bitbucket.org/ihar76/parsergoplugins/types"
	"fmt"
)
type second string

func main(){

}

func (s second) GetCode() string {
	return "AV"
}

func (s second) GetParams() types.Params {
	return types.Params{}
}

func (s second) Run() []types.Trip {

	fmt.Println(s.GetCode() + " parse")
	return []types.Trip{}
}

var Parser second

package main

import (
	types "bitbucket.org/ihar76/parsergoplugins/types"
	"time"
	"strings"
	"fmt"
	"strconv"
	"os"
	"bytes"
)

func (r *ResponseMainData) startParse(direction int, ID string, cabins []string) []types.Trip {

	cabinsMap := map[int]string{}

	for k, column := range r.Availability.Trip.Columns {
		if column.AllCabinButtonText == "" {
			cabin := setCabin(column.Cabin, false)
			cabinsMap[k] = ""
			for _, v := range cabins {
				if v == cabin {
					cabinsMap[k] = cabin
					break
				}
			}
		}
	}

	chTrip := make(chan types.Trip)
	chFin := make(chan bool)
	trips := []types.Trip{}
	go types.SaveTrip(chTrip, &trips)

	for _, fFlight := range r.Availability.Trip.FlattenedFlights {
		go fFlight.parseData(cabinsMap, direction, chTrip, chFin)

	}

	types.CheckCount(len(r.Availability.Trip.FlattenedFlights), chFin)
	close(chTrip)

	if len(trips) > 0 {
		fmt.Println("ok1: " + strconv.Itoa(len(trips)))
		return trips//types.SendToHTTP(trips, ID)
	}
	return nil
}

func (ff FlattenFlight) parseData(cabinsMap map[int]string, direction int, ch chan types.Trip, chFin chan bool) {

	flights := make([]types.Flight,len(ff.Flights))
	prices := map[int]types.Price{}
	//productsMap := map[string]string{}
	cabins := map[int][]string{}
	carriers := []string{}
	flightLegs := []string{}
	flightNumbers := []string{}
	layovers := []string{}
	stops := []string{} // ArriveCode
	totalCabin := []string{}


	for i, f := range ff.Flights {
		flight := ff.fillFlight(i)
		//usaDomestic := (f.OriginCountryCode == f.DestinationCountryCode) && (f.OriginCountryCode == "US")
		flights[i] = flight
		layovers = append(layovers, flight.Layover)
		if i < (len(ff.Flights) - 1) {
			stops = append(stops, flight.ArriveCode)
		}
		carriers = append(carriers, flight.CarrierCode)
		flightLegs = append(flightLegs, flight.FlightDuration)
		flightNumbers = append(flightNumbers, flight.FlightNumber)

		for cabinMap, cabin := range cabinsMap {

			if cabin==""{
				continue
			}

			if len(f.ShoppingProducts) == 0 {

				f.ShoppingProducts = ff.Flights[0].ShoppingProducts
			}

			if len(f.ShoppingProducts)>cabinMap && f.ShoppingProducts[cabinMap].IsMixedCabin == true {
				totalCabin = append (totalCabin, setCabin(f.ShoppingProducts[cabinMap].CabinType, false))
				cabin = totalCabin[cabinMap]
			} else {
				totalCabin = append(totalCabin, cabin)
			}


			flights[i].Cabin = cabin

			if cabin != "" {
				if f.ShoppingProducts[cabinMap].Price == "Not available" {
					if cabinMap > 0 {
						cabins[cabinMap] = append(cabins[cabinMap], cabin)
					}
					continue
				}
				product := f.ShoppingProducts[cabinMap]

				if cabins[cabinMap] == nil {
					cabins[cabinMap] = []string{}
				}
				cabins[cabinMap] = append(cabins[cabinMap], cabin)

				if _, ok := prices[cabinMap]; !ok {
					price := string([]byte(product.Price)[3:])
					prices[cabinMap] = types.Price{
						Cabin: cabin,
						Award: types.Award{
							Type:  strings.ToUpper(product.AwardType),
							Miles: product.PriceAmount,
							Tax:   price}}

				}

			}

		}
	}

	for _, validPrice := range prices {

		trip := types.Trip{
			os.Getenv("PARSER_CODE"),
			direction,
			1,
			flights,
			flights[0].DepartCode,
			flights[0].DepartPlace,
			flights[len(flights)-1].ArriveCode,
			flights[len(flights)-1].ArrivePlace,
			flights[0].DepartDate,
			carriers,
			totalCabin,
			flightLegs,
			flightNumbers,
			layovers,
			stops,
			validPrice.Award.Miles,
			validPrice.Award.Tax,
			validPrice.Award.Type,
			convertHumonHI(ff.Flights[0].TotalTravelTime),
			returnFirstFromUniq(carriers, "MIXED"),
			returnFirstFromUniq(totalCabin, "M"),
			"USD",
		}

		ch <- trip
	}

	chFin <- true


}

func returnFirstFromUniq(sl []string, other string) string {
	if len(sl) == 0 {
		return other
	}
	encountered := map[string]bool{}

	for _, v := range sl {
		if _, ok := encountered[v]; ok != true {
			// Do not add duplicate.
			encountered[v] = true
			if len(encountered) > 1 {
				return other
			}
		}
	}
	// Return the new slice.
	return sl[0]
}

func (f FlattenFlight) fillFlight(position int) types.Flight {

	parseFormat := "01/02/2006 15:04 PM"
	parseDepartFormat := "Mon., Jan. 2, 2006 15:04pm"

	departMoment, _ := time.Parse(parseDepartFormat, f.Flights[position].DepartDate + " " + f.Flights[position].DepartTime)
	arriveMoment, _ := time.Parse(parseFormat, f.Flights[position].ArrivalDateTime)

	return types.Flight{
		Parser:         os.Getenv("PARSER_CODE"),
		Aircraft:       f.Flights[position].EquipmentDisclosures.EquipmentDescription,
		Position:       position,
		ArriveCode:     f.Flights[position].Destination,
		ArriveDate:     arriveMoment.Format("2006-01-02"),
		ArriveTime:     arriveMoment.Format("15:04"),
		DepartCode:     f.Flights[position].Origin,
		DepartDate:     departMoment.Format("2006-01-02"),
		DepartTime:     departMoment.Format("15:04"),
		Layover:        convertHumonHI(f.Flights[position].ConnectTimeMinutes),
		ArrivePlace:    f.Flights[position].DestinationDescription,
		CarrierCode:    f.Flights[position].MarketingCarrier,
		CarrierName:    f.Flights[position].MarketingCarrierDescription,
		DepartPlace:    f.Flights[position].OriginDescription,
		FlightNumber:   f.Flights[position].MarketingCarrier + " " + f.Flights[position].FlightNumber,
		FlightDuration: convertHumonHI(f.Flights[position].TravelTime),
	}
}

func convertHumonHILogic(t string) string {
	parseHumanFormat := "15 4"

	if(len(t)>0) {
		outFormat := bytes.Split([]byte(t), []byte(" "))
		if(len(outFormat)>1) {
			outFormat[0] = outFormat[0][:len(outFormat[0])-1]
		} else {
			outFormat = append(outFormat, outFormat[0])
			outFormat[0] = []byte("0")
		}
		outFormat[1] = outFormat[1][:len(outFormat[1])-1]
		t = string(bytes.Join(outFormat, []byte(" ")))
	}


	dt, _ := time.Parse(parseHumanFormat, t)

	tdfin := dt.Format("15:04")
	return tdfin
}

func convertHumonHI(t string) string {
	if len(t)<5 {
		return convertHumonHILogic(t)
	}

	tr := strings.Split(t, " ")
	return tr[0][:len(tr[0])-1] + ":" + tr[1][:len(tr[1])-1]

}
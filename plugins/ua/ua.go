package main

import (
	"bitbucket.org/ihar76/parsergoplugins/types"
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"encoding/json"
	"time"
	"interfaceExample/classChild"
	"os"
)

type first string

func (f first) GetCode() string {
	return "UA"
}

// for single mode

var (

	InParams  Params
	t1        time.Time
	trips     []classChild.Trip
)

func Handle(body []byte) {
	json.Unmarshal(body, &InParams)
	t1 = time.Now();
	fmt.Printf("Elapsed time: %v\n", time.Since(t1));
	t1 = time.Now()
	InParams.StartProcess(string(body))
}


func main() {
	fmt.Println("env db_username: " + os.Getenv("DB_USERNAME"))
	types.ServeHTTP(Handle)
}

func (p Params) StartProcess(paramsStart string) {
	f := first("")
	proxies := types.GetProxies("UA")
	trips := f.Run([]byte(paramsStart), proxies.GetProxy())
	if trips == nil {

	}

	types.SendToHTTP(trips, p.ID)

}
// end single mode

func (f first) Run(str []byte, proxy types.Proxy) []types.Trip {

	fmt.Println(f.GetCode() + " parse")
	params := Params{}
	json.Unmarshal(str, &params)
	trips := []types.Trip{}
	for _, slice := range params.Slices {
		slice.Proxy = proxy
		slice.Passengers = params.Passengers
		trips = append(trips, slice.RunTravel(params.ID, params.Cabins, params)...)
	}

	return trips
}

func (t *Slice) RunTravel(ID string, cabins []string, query Params) []types.Trip {
	trips := []types.Trip{}

	query.UUID = query.ID
	queryBytes, _ := json.Marshal(query)
	for _, date := range t.Dates {
		trips = append(trips, t.SearchByDay(date, ID, cabins)...)
	}

	go types.SendSlaves(trips, os.Getenv("PARSER_CODE"), ID, string(queryBytes))
	return trips
}

var Parser first

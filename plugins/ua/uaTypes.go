package main

import (
	types "bitbucket.org/ihar76/parsergoplugins/types"
	"strings"
)

var Headers = map[string]string{
	"Content-Type":    "application/json; charset=UTF-8",
	"User-Agent":      "Dalvik/2.1.0 (Linux; U; Android 7.0; ASUS_A001 Build/NRD90M)",
	"Host":            "smartphone.united.com",
	"Connection":      "keep-alive",
	"Accept-Encoding": "gzip"}

type (
	Params struct {
		types.Params
		Slices []Slice `json:"slices"`
	}

	Date struct {
		types.Date
	}

	Slice struct {
		types.Slice
		Dates []Date `json:"dates"`
	}

	Trip struct {
		Cabin                           string      `json:"cabin"`
		ChangeType                      int         `json:"changeType"`
		DepartDate                      string      `json:"departDate"`
		Destination                     string      `json:"destination"`
		Origin                          string      `json:"origin"`
		SearchFiltersIn                 interface{} `json:"searchFiltersIn"`
		SearchFiltersOut                interface{} `json:"searchFiltersOut"`
		SearchNearbyDestinationAirports bool        `json:"searchNearbyDestinationAirports"`
		SearchNearbyOriginAirports      bool        `json:"searchNearbyOriginAirports"`
		ShareMessage                    interface{} `json:"shareMessage"`
		UseFilters                      bool        `json:"useFilters"`
	}

	ApplicationRequest struct {
		ID           int    `json:"id"`
		IsProduction bool   `json:"isProduction"`
		Name         string `json:"name"`
		Version struct {
			Build       interface{} `json:"build"`
			DisplayText interface{} `json:"displayText"`
			Major       string      `json:"major"`
			Minor       string      `json:"minor"`
		} `json:"version"`
	}

	GetRequestData struct {
		AccessCode               string             `json:"accessCode"`
		Application              ApplicationRequest `json:"application"`
		AwardTravel              bool               `json:"awardTravel"`
		CameFromFSRHandler       bool               `json:"cameFromFSRHandler"`
		ClassOfServices          interface{}        `json:"classOfServices"`
		CountryCode              string             `json:"countryCode"`
		CustomerMetrics          interface{}        `json:"customerMetrics"`
		DeviceID                 string             `json:"deviceId"`
		EmployeeDiscountID       string             `json:"employeeDiscountId"`
		FareClass                string             `json:"fareClass"`
		FareType                 string             `json:"fareType"`
		GetFlightsWithStops      bool               `json:"getFlightsWithStops"`
		GetNonStopFlightsOnly    bool               `json:"getNonStopFlightsOnly"`
		IsCorporateBooking       bool               `json:"isCorporateBooking"`
		IsELFFareDisplayAtFSR    bool               `json:"isELFFareDisplayAtFSR"`
		IsReshopChange           bool               `json:"isReshopChange"`
		LanguageCode             string             `json:"languageCode"`
		LastName                 interface{}        `json:"lastName"`
		LengthOfCalendar         int                `json:"lengthOfCalendar"`
		MaxNumberOfStops         int                `json:"maxNumberOfStops"`
		MaxNumberOfTrips         int                `json:"maxNumberOfTrips"`
		MileagePlusAccountNumber string             `json:"mileagePlusAccountNumber"`
		MobCPCorporateDetails struct {
			CorporateCompanyName    interface{} `json:"corporateCompanyName"`
			CorporateTravelProvider interface{} `json:"corporateTravelProvider"`
			DiscountCode            interface{} `json:"discountCode"`
			FareGroupID             interface{} `json:"fareGroupId"`
		} `json:"mobCPCorporateDetails"`
		NumberOfAdults         int         `json:"numberOfAdults"`
		NumberOfChildren12To17 int         `json:"numberOfChildren12To17"`
		NumberOfChildren2To4   int         `json:"numberOfChildren2To4"`
		NumberOfChildren5To11  int         `json:"numberOfChildren5To11"`
		NumberOfInfantOnLap    int         `json:"numberOfInfantOnLap"`
		NumberOfInfantWithSeat int         `json:"numberOfInfantWithSeat"`
		NumberOfSeniors        int         `json:"numberOfSeniors"`
		PremierStatusLevel     int         `json:"premierStatusLevel"`
		PromotionCode          interface{} `json:"promotionCode"`
		RecordLocator          interface{} `json:"recordLocator"`
		ReshopSegments         interface{} `json:"reshopSegments"`
		ReshopTravelers        interface{} `json:"reshopTravelers"`
		ResultSortType         string      `json:"resultSortType"`
		SearchType             string      `json:"searchType"`
		ServiceType            string      `json:"serviceType"`
		SessionID              string      `json:"sessionId"`
		ShowMileageDetails     bool        `json:"showMileageDetails"`
		TransactionID          string      `json:"transactionId"`
		Trips                  []Trip      `json:"trips"`
	}

	GetRequestDataSecond struct {
		AccessCode             string             `json:"accessCode"`
		Application            ApplicationRequest `json:"application"`
		DeviceID               string             `json:"deviceId"`
		LanguageCode           string             `json:"languageCode"`
		LastTripIndexRequested int                `json:"lastTripIndexRequested"`
		SearchFiltersIn struct {
			AircraftTypes         string   `json:"aircraftTypes"`
			AirportsDestination   string   `json:"airportsDestination"`
			AirportsOrigin        string   `json:"airportsOrigin"`
			AirportsStopToAvoid   string   `json:"airportsStopToAvoid"`
			BookingCodes          string   `json:"bookingCodes"`
			CabinCountMax         int      `json:"cabinCountMax"`
			CabinCountMin         int      `json:"cabinCountMin"`
			CarrierDefault        bool     `json:"carrierDefault"`
			CarrierExpress        bool     `json:"carrierExpress"`
			CarrierPartners       bool     `json:"carrierPartners"`
			CarrierStar           bool     `json:"carrierStar"`
			DurationMax           int      `json:"durationMax"`
			DurationMin           int      `json:"durationMin"`
			DurationStopMax       int      `json:"durationStopMax"`
			DurationStopMin       int      `json:"durationStopMin"`
			FareFamily            string   `json:"fareFamily"`
			FilterSortPaging      bool     `json:"filterSortPaging"`
			PageNumber            int      `json:"pageNumber"`
			PriceMax              float64  `json:"priceMax"`
			PriceMin              float64  `json:"priceMin"`
			ShowArrivalFilters    bool     `json:"showArrivalFilters"`
			ShowDepartureFilters  bool     `json:"showDepartureFilters"`
			ShowDurationFilters   bool     `json:"showDurationFilters"`
			ShowLayOverFilters    bool     `json:"showLayOverFilters"`
			ShowPriceFilters      bool     `json:"showPriceFilters"`
			ShowSortingandFilters bool     `json:"showSortingandFilters"`
			SortType1             string   `json:"sortType1"`
			StopCountExcl         int      `json:"stopCountExcl"`
			StopCountMax          int      `json:"stopCountMax"`
			StopCountMin          int      `json:"stopCountMin"`
			WarningsFilter        []string `json:"warningsFilter"`
		} `json:"searchFiltersIn"`
		SessionID string `json:"sessionId"`
	}

	FlattenFlight struct {
		TripID       string `json:"tripId"`
		FlightID     string `json:"flightId"`
		ProductID    string `json:"productId"`
		TripDays     string `json:"tripDays"`
		CabinMessage string `json:"cabinMessage"`
		Flights []struct {
			TripID                 string      `json:"tripId"`
			FlightID               string      `json:"flightId"`
			ProductID              string      `json:"productId"`
			Airfare                float64     `json:"airfare"`
			AirfareDisplayValue    string      `json:"airfareDisplayValue"`
			SeatsRemaining         int         `json:"seatsRemaining"`
			Cabin                  string      `json:"cabin"`
			ChangeOfGauge          bool        `json:"changeOfGauge"`
			GaugeChanges           interface{} `json:"gaugeChanges"`
			Connections            interface{} `json:"connections"`
			ConnectTimeMinutes     string      `json:"connectTimeMinutes"`
			DepartDate             string      `json:"departDate"`
			DepartTime             string      `json:"departTime"`
			Destination            string      `json:"destination"`
			DestinationDate        string      `json:"destinationDate"`
			DestinationTime        string      `json:"destinationTime"`
			DestinationDescription string      `json:"destinationDescription"`
			DestinationCountryCode string      `json:"destinationCountryCode"`
			EquipmentDisclosures struct {
				EquipmentDescription  string `json:"equipmentDescription"`
				EquipmentType         string `json:"equipmentType"`
				IsSingleCabin         bool   `json:"isSingleCabin"`
				NoBoardingAssistance  bool   `json:"noBoardingAssistance"`
				NonJetEquipment       bool   `json:"nonJetEquipment"`
				WheelchairsNotAllowed bool   `json:"wheelchairsNotAllowed"`
			} `json:"equipmentDisclosures"`
			FareBasisCode               string      `json:"fareBasisCode"`
			FlightNumber                string      `json:"flightNumber"`
			GroundTime                  string      `json:"groundTime"`
			InternationalCity           string      `json:"internationalCity"`
			IsCheapestAirfare           bool        `json:"isCheapestAirfare"`
			IsConnection                bool        `json:"isConnection"`
			MarketingCarrier            string      `json:"marketingCarrier"`
			MarketingCarrierDescription string      `json:"marketingCarrierDescription"`
			Miles                       string      `json:"miles"`
			OnTimePerformance           interface{} `json:"onTimePerformance"`
			OperatingCarrier            string      `json:"operatingCarrier"`
			OperatingCarrierDescription string      `json:"operatingCarrierDescription"`
			Origin                      string      `json:"origin"`
			OriginDescription           string      `json:"originDescription"`
			OriginCountryCode           string      `json:"originCountryCode"`
			Rewards                     interface{} `json:"rewards"`
			RewardPriceSummaries        interface{} `json:"rewardPriceSummaries"`
			Selected                    bool        `json:"selected"`
			StopDestination             string      `json:"stopDestination"`
			StopInfos                   interface{} `json:"stopInfos"`
			Stops                       int         `json:"stops"`
			TravelTime                  string      `json:"travelTime"`
			TotalTravelTime             string      `json:"totalTravelTime"`
			Messages []struct {
				TripID            string      `json:"tripId"`
				FlightID          string      `json:"flightId"`
				ConnectionIndex   string      `json:"connectionIndex"`
				FlightNumberField string      `json:"flightNumberField"`
				MessageCode       string      `json:"messageCode"`
				MessageParameters interface{} `json:"messageParameters"`
			} `json:"messages"`
			Meal                        string `json:"meal"`
			FpwSAir                     bool   `json:"fpwSAir"`
			ServiceClass                string `json:"serviceClass"`
			ServiceClassDescription     string `json:"serviceClassDescription"`
			EpaMessageTitle             string `json:"epaMessageTitle"`
			EpaMessage                  string `json:"epaMessage"`
			ShowEPAMessage              bool   `json:"showEPAMessage"`
			IsCheckInWindow             bool   `json:"isCheckInWindow"`
			CheckInWindowText           string `json:"checkInWindowText"`
			DepartureDateTime           string `json:"departureDateTime"`
			ArrivalDateTime             string `json:"arrivalDateTime"`
			DepartureDateTimeGMT        string `json:"departureDateTimeGMT"`
			ArrivalDateTimeGMT          string `json:"arrivalDateTimeGMT"`
			MatchServiceClassRequested  bool   `json:"matchServiceClassRequested"`
			HasWifi                     bool   `json:"hasWifi"`
			HasInSeatPower              bool   `json:"hasInSeatPower"`
			HasDirecTV                  bool   `json:"hasDirecTV"`
			HasAVOnDemand               bool   `json:"hasAVOnDemand"`
			HasBeverageService          bool   `json:"hasBeverageService"`
			HasEconomyLieFlatSeating    bool   `json:"hasEconomyLieFlatSeating"`
			HasEconomyMeal              bool   `json:"hasEconomyMeal"`
			HasFirstClassMeal           bool   `json:"hasFirstClassMeal"`
			HasFirstClassLieFlatSeating bool   `json:"hasFirstClassLieFlatSeating"`
			ShoppingProducts []struct {
				ProductID                 string      `json:"productId"`
				Type                      string      `json:"type"`
				SubType                   string      `json:"subType"`
				LongCabin                 string      `json:"longCabin"`
				Cabin                     string      `json:"cabin"`
				Description               string      `json:"description"`
				Price                     string      `json:"price"`
				MilesDisplayValue         string      `json:"milesDisplayValue"`
				PriceAmount               float64     `json:"priceAmount"`
				MilesDisplayAmount        float64     `json:"milesDisplayAmount"`
				Meal                      string      `json:"meal"`
				ProductDetail             interface{} `json:"productDetail"`
				IsMixedCabin              bool        `json:"isMixedCabin"`
				MixedCabinSegmentMessages interface{} `json:"mixedCabinSegmentMessages"`
				AwardType                 string      `json:"awardType"`
				AllCabinButtonText        string      `json:"allCabinButtonText"`
				IsSelectedCabin           bool        `json:"isSelectedCabin"`
				MileageButton             int         `json:"mileageButton"`
				SeatsRemaining            int         `json:"seatsRemaining"`
				PqdText                   string      `json:"pqdText"`
				PqmText                   string      `json:"pqmText"`
				RdmText                   string      `json:"rdmText"`
				IsPremierCabinSaver       bool        `json:"isPremierCabinSaver"`
				IsUADiscount              bool        `json:"isUADiscount"`
				IsELF                     bool        `json:"isELF"`
				CabinType                 string      `json:"cabinType"`
				PriceFromText             string      `json:"priceFromText"`
				IsIBELite                 bool        `json:"isIBELite"`
				ShortProductName          interface{} `json:"shortProductName"`
				ProductCode               string      `json:"productCode"`
			} `json:"shoppingProducts"`
			SegID                    string      `json:"segID"`
			SegNumber                int         `json:"segNumber"`
			FlightDepartureDays      string      `json:"flightDepartureDays"`
			FlightArrivalDays        string      `json:"flightArrivalDays"`
			CorporateFareIndicator   string      `json:"corporateFareIndicator"`
			IsAddCollectWaived       bool        `json:"isAddCollectWaived"`
			AddCollectProductID      interface{} `json:"addCollectProductId"`
			FlightHash               string      `json:"flightHash"`
			MilesDisplayValue        string      `json:"milesDisplayValue"`
			CabinDisclaimer          string      `json:"cabinDisclaimer"`
			AvailSeatsDisclaimer     string      `json:"availSeatsDisclaimer"`
			PreferredCabinName       string      `json:"preferredCabinName"`
			PreferredCabinMessage    string      `json:"preferredCabinMessage"`
			OvernightConnection      string      `json:"overnightConnection"`
			ShowSeatMap              bool        `json:"showSeatMap"`
			IsStopOver               bool        `json:"isStopOver"`
			PqdText                  string      `json:"pqdText"`
			PqmText                  string      `json:"pqmText"`
			RdmText                  string      `json:"rdmText"`
			YqyrMessage              string      `json:"yqyrMessage"`
			GovtMessage              string      `json:"govtMessage"`
			IsAwardSaver             bool        `json:"isAwardSaver"`
			RedEyeFlightDepDate      string      `json:"redEyeFlightDepDate"`
			NextDayFlightArrDate     string      `json:"nextDayFlightArrDate"`
			FlightDateChanged        bool        `json:"flightDateChanged"`
			BookingClassAvailability interface{} `json:"bookingClassAvailability"`
			TripIndex                int         `json:"tripIndex"`
		} `json:"flights"`
		IsUADiscount        bool        `json:"isUADiscount"`
		IsAddCollectWaived  bool        `json:"isAddCollectWaived"`
		AddCollectProductID interface{} `json:"addCollectProductId"`
		FlightHash          string      `json:"flightHash"`
		IsIBELite           bool        `json:"isIBELite"`
	}

	keyVal struct {
		Key          string `json:"key"`
		Value        string `json:"value"`
		DisplayValue string `json:"displayValue"`
		Amount       string `json:"amount"`
		Currency     string `json:"currency"`
		IsSelected   bool   `json:"isSelected"`
	}

	FareFamily struct {
		FareFamily        string `json:"fareFamily"`
		MaxMileage        int    `json:"maxMileage"`
		MaxPrice          string `json:"maxPrice"`
		MinMileage        int    `json:"minMileage"`
		MinPrice          string `json:"minPrice"`
		MinPriceInSummary bool   `json:"minPriceInSummary"`
	}

	SearchFilters struct {
		AircraftTypes           string        `json:"aircraftTypes"`
		AirportsDestination     string        `json:"airportsDestination"`
		AirportsDestinationList interface{}   `json:"airportsDestinationList"`
		AirportsOrigin          string        `json:"airportsOrigin"`
		AirportsOriginList      interface{}   `json:"airportsOriginList"`
		AirportsStop            string        `json:"airportsStop"`
		AirportsStopList        []keyVal      `json:"airportsStopList"`
		AirportsStopToAvoid     string        `json:"airportsStopToAvoid"`
		AirportsStopToAvoidList interface{}   `json:"airportsStopToAvoidList"`
		BookingCodes            string        `json:"bookingCodes"`
		CabinCountMax           int           `json:"cabinCountMax"`
		CabinCountMin           int           `json:"cabinCountMin"`
		CarrierDefault          bool          `json:"carrierDefault"`
		CarrierExpress          bool          `json:"carrierExpress"`
		CarrierPartners         bool          `json:"carrierPartners"`
		CarriersMarketing       string        `json:"carriersMarketing"`
		CarriersMarketingList   []keyVal      `json:"carriersMarketingList"`
		CarriersOperating       string        `json:"carriersOperating"`
		CarriersOperatingList   []keyVal      `json:"carriersOperatingList"`
		CarrierStar             bool          `json:"carrierStar"`
		DurationMax             int           `json:"durationMax"`
		DurationMin             int           `json:"durationMin"`
		DurationStopMax         int           `json:"durationStopMax"`
		DurationStopMin         int           `json:"durationStopMin"`
		EquipmentCodes          string        `json:"equipmentCodes"`
		EquipmentList           []keyVal      `json:"equipmentList"`
		EquipmentTypes          string        `json:"equipmentTypes"`
		FareFamilies            []FareFamily  `json:"fareFamilies"`
		FareFamily              string        `json:"fareFamily"`
		PriceMax                float64       `json:"priceMax"`
		PriceMin                float64       `json:"priceMin"`
		PriceMaxDisplayValue    string        `json:"priceMaxDisplayValue"`
		PriceMinDisplayValue    string        `json:"priceMinDisplayValue"`
		StopCountExcl           int           `json:"stopCountExcl"`
		StopCountMax            int           `json:"stopCountMax"`
		StopCountMin            int           `json:"stopCountMin"`
		TimeArrivalMax          string        `json:"timeArrivalMax"`
		TimeArrivalMin          string        `json:"timeArrivalMin"`
		TimeDepartMax           string        `json:"timeDepartMax"`
		TimeDepartMin           string        `json:"timeDepartMin"`
		Warnings                interface{}   `json:"warnings"`
		WarningsFilter          []interface{} `json:"warningsFilter"`
		PageNumber              int           `json:"pageNumber"`
		SortType1               string        `json:"sortType1"`
		SortTypes               []keyVal      `json:"sortTypes"`
		NumberofStops           []keyVal      `json:"numberofStops"`
		AmenityTypes            []keyVal      `json:"amenityTypes"`
		CarrierTypes            []keyVal      `json:"carrierTypes"`
		AircraftCabinTypes      []keyVal      `json:"aircraftCabinTypes"`
		ShowPriceFilters        bool          `json:"showPriceFilters"`
		ShowDepartureFilters    bool          `json:"showDepartureFilters"`
		ShowArrivalFilters      bool          `json:"showArrivalFilters"`
		ShowDurationFilters     bool          `json:"showDurationFilters"`
		ShowLayOverFilters      bool          `json:"showLayOverFilters"`
		ShowSortingandFilters   bool          `json:"showSortingandFilters"`
		FilterSortPaging        bool          `json:"filterSortPaging"`
	}

	ResponseMainData struct {
		ShopRequest struct {
			SessionID   string `json:"sessionId"`
			CountryCode string `json:"countryCode"`
			Trips []struct {
				Origin                          string      `json:"origin"`
				Destination                     string      `json:"destination"`
				DepartDate                      string      `json:"departDate"`
				ArrivalDate                     interface{} `json:"arrivalDate"`
				Cabin                           string      `json:"cabin"`
				UseFilters                      bool        `json:"useFilters"`
				SearchFiltersIn                 interface{} `json:"searchFiltersIn"`
				SearchFiltersOut                interface{} `json:"searchFiltersOut"`
				SearchNearbyOriginAirports      bool        `json:"searchNearbyOriginAirports"`
				SearchNearbyDestinationAirports bool        `json:"searchNearbyDestinationAirports"`
				ShareMessage                    interface{} `json:"shareMessage"`
				ChangeType                      string      `json:"changeType"`
			} `json:"trips"`
			NumberOfAdults           int         `json:"numberOfAdults"`
			NumberOfSeniors          int         `json:"numberOfSeniors"`
			NumberOfChildren2To4     int         `json:"numberOfChildren2To4"`
			NumberOfChildren5To11    int         `json:"numberOfChildren5To11"`
			NumberOfChildren12To17   int         `json:"numberOfChildren12To17"`
			NumberOfInfantOnLap      int         `json:"numberOfInfantOnLap"`
			NumberOfInfantWithSeat   int         `json:"numberOfInfantWithSeat"`
			AwardTravel              bool        `json:"awardTravel"`
			MileagePlusAccountNumber string      `json:"mileagePlusAccountNumber"`
			SearchType               string      `json:"searchType"`
			ServiceType              string      `json:"serviceType"`
			MaxNumberOfStops         int         `json:"maxNumberOfStops"`
			MaxNumberOfTrips         int         `json:"maxNumberOfTrips"`
			ResultSortType           string      `json:"resultSortType"`
			FareType                 string      `json:"fareType"`
			ClassOfServices          interface{} `json:"classOfServices"`
			PageIndex                int         `json:"pageIndex"`
			PageSize                 int         `json:"pageSize"`
			FareClass                string      `json:"fareClass"`
			PromotionCode            interface{} `json:"promotionCode"`
			ShowMileageDetails       bool        `json:"showMileageDetails"`
			PremierStatusLevel       int         `json:"premierStatusLevel"`
			EmployeeDiscountID       string      `json:"employeeDiscountId"`
			IsReshopChange           bool        `json:"isReshopChange"`
			RecordLocator            interface{} `json:"recordLocator"`
			LastName                 interface{} `json:"lastName"`
			ReshopTravelers          interface{} `json:"reshopTravelers"`
			ReshopSegments           interface{} `json:"reshopSegments"`
			IsCorporateBooking       bool        `json:"isCorporateBooking"`
			MobCPCorporateDetails struct {
				DiscountCode            interface{} `json:"discountCode"`
				CorporateCompanyName    interface{} `json:"corporateCompanyName"`
				CorporateTravelProvider interface{} `json:"corporateTravelProvider"`
				FareGroupID             interface{} `json:"fareGroupId"`
			} `json:"mobCPCorporateDetails"`
			CustomerMetrics       interface{} `json:"customerMetrics"`
			LengthOfCalendar      int         `json:"lengthOfCalendar"`
			GetNonStopFlightsOnly bool        `json:"getNonStopFlightsOnly"`
			GetFlightsWithStops   bool        `json:"getFlightsWithStops"`
			CameFromFSRHandler    bool        `json:"cameFromFSRHandler"`
			IsELFFareDisplayAtFSR bool        `json:"isELFFareDisplayAtFSR"`
			IsReshop              bool        `json:"isReshop"`
			AccessCode            string      `json:"accessCode"`
			TransactionID         string      `json:"transactionId"`
			LanguageCode          string      `json:"languageCode"`
			Application struct {
				ID   int    `json:"id"`
				Name string `json:"name"`
				Version struct {
					DisplayText interface{} `json:"displayText"`
					Major       string      `json:"major"`
					Minor       string      `json:"minor"`
					Build       interface{} `json:"build"`
				} `json:"version"`
				IsProduction bool `json:"isProduction"`
			} `json:"application"`
			DeviceID string `json:"deviceId"`
		} `json:"shopRequest"`
		Availability struct {
			SessionID         string  `json:"sessionId"`
			CartID            string  `json:"cartId"`
			CloseInBookingFee float64 `json:"closeInBookingFee"`
			FareWheel []struct {
				Key          string `json:"key"`
				TripID       string `json:"tripId"`
				ProductID    string `json:"productId"`
				DisplayValue string `json:"displayValue"`
				Value        string `json:"value"`
			} `json:"fareWheel"`
			Trip struct {
				TripID                 string          `json:"tripId"`
				OriginDecoded          string          `json:"originDecoded"`
				DestinationDecoded     string          `json:"destinationDecoded"`
				FlightCount            int             `json:"flightCount"`
				TotalFlightCount       int             `json:"totalFlightCount"`
				FlattenedFlights       []FlattenFlight `json:"flattenedFlights"`
				FlightSections         interface{}     `json:"flightSections"`
				LastTripIndexRequested int             `json:"lastTripIndexRequested"`
				Columns []struct {
					ProductID          string  `json:"productId"`
					Type               string  `json:"type"`
					SubType            string  `json:"subType"`
					LongCabin          string  `json:"longCabin"`
					Cabin              string  `json:"cabin"`
					Description        string  `json:"description"`
					Price              string  `json:"price"`
					MilesDisplayValue  string  `json:"milesDisplayValue"`
					PriceAmount        float64 `json:"priceAmount"`
					MilesDisplayAmount float64 `json:"milesDisplayAmount"`
					Meal               string  `json:"meal"`
					ProductDetail struct {
						Title          string      `json:"title"`
						Header         string      `json:"header"`
						Body           string      `json:"body"`
						ProductDetails interface{} `json:"productDetails"`
						PqdText        string      `json:"pqdText"`
						PqmText        string      `json:"pqmText"`
						RdmText        string      `json:"rdmText"`
						ProductCabinMessages []struct {
							Segments     string `json:"segments"`
							Cabin        string `json:"cabin"`
							IsMixedCabin bool   `json:"isMixedCabin"`
						} `json:"productCabinMessages"`
					} `json:"productDetail"`
					IsMixedCabin              bool        `json:"isMixedCabin"`
					MixedCabinSegmentMessages []string    `json:"mixedCabinSegmentMessages"`
					AwardType                 string      `json:"awardType"`
					AllCabinButtonText        string      `json:"allCabinButtonText"`
					IsSelectedCabin           bool        `json:"isSelectedCabin"`
					MileageButton             int         `json:"mileageButton"`
					SeatsRemaining            int         `json:"seatsRemaining"`
					PqdText                   string      `json:"pqdText"`
					PqmText                   string      `json:"pqmText"`
					RdmText                   string      `json:"rdmText"`
					IsPremierCabinSaver       bool        `json:"isPremierCabinSaver"`
					IsUADiscount              bool        `json:"isUADiscount"`
					IsELF                     bool        `json:"isELF"`
					CabinType                 string      `json:"cabinType"`
					PriceFromText             string      `json:"priceFromText"`
					IsIBELite                 bool        `json:"isIBELite"`
					ShortProductName          interface{} `json:"shortProductName"`
					ProductCode               interface{} `json:"productCode"`
				} `json:"columns"`
				YqyrMessage                     string        `json:"yqyrMessage"`
				PageCount                       int           `json:"pageCount"`
				CallDurationText                string        `json:"callDurationText"`
				FlightDateChangeMessage         string        `json:"flightDateChangeMessage"`
				TripHasNonStopflightsOnly       bool          `json:"tripHasNonStopflightsOnly"`
				Origin                          string        `json:"origin"`
				Destination                     string        `json:"destination"`
				DepartDate                      string        `json:"departDate"`
				ArrivalDate                     string        `json:"arrivalDate"`
				Cabin                           string        `json:"cabin"`
				UseFilters                      bool          `json:"useFilters"`
				SearchFiltersIn                 SearchFilters `json:"searchFiltersIn"`
				SearchFiltersOut                SearchFilters `json:"searchFiltersOut"`
				SearchNearbyOriginAirports      bool          `json:"searchNearbyOriginAirports"`
				SearchNearbyDestinationAirports bool          `json:"searchNearbyDestinationAirports"`
				ShareMessage                    string        `json:"shareMessage"`
				ChangeType                      string        `json:"changeType"`
			} `json:"trip"`
			Reservation          interface{} `json:"reservation"`
			FareLock             interface{} `json:"fareLock"`
			AwardCalendar        interface{} `json:"awardCalendar"`
			AwardDynamicCalendar interface{} `json:"awardDynamicCalendar"`
			CallDuration         string      `json:"callDuration"`
			UaDiscount           string      `json:"uaDiscount"`
			AwardTravel          bool        `json:"awardTravel"`
			ElfShopMessages []struct {
				ID            string `json:"id"`
				CurrentValue  string `json:"currentValue"`
				SaveToPersist bool   `json:"saveToPersist"`
			} `json:"elfShopMessages"`
			ElfShopOptions []struct {
				OptionDescription         string      `json:"optionDescription"`
				AvailableInElf            bool        `json:"availableInElf"`
				AvailableInEconomy        bool        `json:"availableInEconomy"`
				OptionIcon                string      `json:"optionIcon"`
				FareSubDescriptionELF     interface{} `json:"fareSubDescriptionELF"`
				FareSubDescriptionEconomy interface{} `json:"fareSubDescriptionEconomy"`
			} `json:"elfShopOptions"`
			OfferMetaSearchElfUpsell bool        `json:"offerMetaSearchElfUpsell"`
			Upsells                  interface{} `json:"upsells"`
			CorporateDisclaimer      interface{} `json:"corporateDisclaimer"`
			DisablePricingBySlice    bool        `json:"disablePricingBySlice"`
			PriceTextDescription     string      `json:"priceTextDescription"`
			FSRFareDescription       string      `json:"fSRFareDescription"`
			FsrAlertMessages []struct {
				HeaderMsg string `json:"headerMsg"`
				BodyMsg   string `json:"bodyMsg"`
				Buttons []struct {
					ButtonLabel string `json:"buttonLabel"`
					UpdatedShopRequest struct {
						SessionID   string `json:"sessionId"`
						CountryCode string `json:"countryCode"`
						Trips []struct {
							Origin                          string      `json:"origin"`
							Destination                     string      `json:"destination"`
							DepartDate                      string      `json:"departDate"`
							ArrivalDate                     interface{} `json:"arrivalDate"`
							Cabin                           string      `json:"cabin"`
							UseFilters                      bool        `json:"useFilters"`
							SearchFiltersIn                 interface{} `json:"searchFiltersIn"`
							SearchFiltersOut                interface{} `json:"searchFiltersOut"`
							SearchNearbyOriginAirports      bool        `json:"searchNearbyOriginAirports"`
							SearchNearbyDestinationAirports bool        `json:"searchNearbyDestinationAirports"`
							ShareMessage                    interface{} `json:"shareMessage"`
							ChangeType                      string      `json:"changeType"`
						} `json:"trips"`
						NumberOfAdults           int         `json:"numberOfAdults"`
						NumberOfSeniors          int         `json:"numberOfSeniors"`
						NumberOfChildren2To4     int         `json:"numberOfChildren2To4"`
						NumberOfChildren5To11    int         `json:"numberOfChildren5To11"`
						NumberOfChildren12To17   int         `json:"numberOfChildren12To17"`
						NumberOfInfantOnLap      int         `json:"numberOfInfantOnLap"`
						NumberOfInfantWithSeat   int         `json:"numberOfInfantWithSeat"`
						AwardTravel              bool        `json:"awardTravel"`
						MileagePlusAccountNumber string      `json:"mileagePlusAccountNumber"`
						SearchType               string      `json:"searchType"`
						ServiceType              string      `json:"serviceType"`
						MaxNumberOfStops         int         `json:"maxNumberOfStops"`
						MaxNumberOfTrips         int         `json:"maxNumberOfTrips"`
						ResultSortType           string      `json:"resultSortType"`
						FareType                 string      `json:"fareType"`
						ClassOfServices          interface{} `json:"classOfServices"`
						PageIndex                int         `json:"pageIndex"`
						PageSize                 int         `json:"pageSize"`
						FareClass                string      `json:"fareClass"`
						PromotionCode            interface{} `json:"promotionCode"`
						ShowMileageDetails       bool        `json:"showMileageDetails"`
						PremierStatusLevel       int         `json:"premierStatusLevel"`
						EmployeeDiscountID       string      `json:"employeeDiscountId"`
						IsReshopChange           bool        `json:"isReshopChange"`
						RecordLocator            interface{} `json:"recordLocator"`
						LastName                 interface{} `json:"lastName"`
						ReshopTravelers          interface{} `json:"reshopTravelers"`
						ReshopSegments           interface{} `json:"reshopSegments"`
						IsCorporateBooking       bool        `json:"isCorporateBooking"`
						MobCPCorporateDetails struct {
							DiscountCode            interface{} `json:"discountCode"`
							CorporateCompanyName    interface{} `json:"corporateCompanyName"`
							CorporateTravelProvider interface{} `json:"corporateTravelProvider"`
							FareGroupID             interface{} `json:"fareGroupId"`
						} `json:"mobCPCorporateDetails"`
						CustomerMetrics       interface{} `json:"customerMetrics"`
						LengthOfCalendar      int         `json:"lengthOfCalendar"`
						GetNonStopFlightsOnly bool        `json:"getNonStopFlightsOnly"`
						GetFlightsWithStops   bool        `json:"getFlightsWithStops"`
						CameFromFSRHandler    bool        `json:"cameFromFSRHandler"`
						IsELFFareDisplayAtFSR bool        `json:"isELFFareDisplayAtFSR"`
						IsReshop              bool        `json:"isReshop"`
						AccessCode            string      `json:"accessCode"`
						TransactionID         string      `json:"transactionId"`
						LanguageCode          string      `json:"languageCode"`
						Application struct {
							ID   int    `json:"id"`
							Name string `json:"name"`
							Version struct {
								DisplayText interface{} `json:"displayText"`
								Major       string      `json:"major"`
								Minor       string      `json:"minor"`
								Build       interface{} `json:"build"`
							} `json:"version"`
							IsProduction bool `json:"isProduction"`
						} `json:"application"`
						DeviceID string `json:"deviceId"`
					} `json:"updatedShopRequest"`
					RedirectURL interface{} `json:"redirectUrl"`
				} `json:"buttons"`
				MessageType     int    `json:"messageType"`
				RestHandlerType string `json:"restHandlerType"`
			} `json:"fsrAlertMessages"`
			AvailableAwardMilesWithDesc interface{} `json:"availableAwardMilesWithDesc"`
		} `json:"availability"`
		Disclaimer         []string          `json:"disclaimer"`
		CartID             string            `json:"cartId"`
		RefreshResultsData string            `json:"refreshResultsData"`
		NoFlightsWithStops bool              `json:"noFlightsWithStops"`
		TransactionID      string            `json:"transactionId"`
		LanguageCode       string            `json:"languageCode"`
		MachineName        string            `json:"machineName"`
		CallDuration       int               `json:"callDuration"`
		Exception          map[string]string `json:"exception"`
	}
)

func (ar *ApplicationRequest) fill() {
	ar.ID = 2
	ar.Name = "Android"
	ar.Version.Major = "2.1.53"
	ar.Version.Minor = "2.1.53"
}

func setCabin(cabinType string, isUSADomestic bool) string {
	cabin := cabinType[:1]
	cabinLType := strings.ToLower(cabinType)

	switch (cabinLType) {
	case "coach",
		"economy",
		"united economy",
		"united polaris economy":
		cabin = "E";
		break;
	case "economy plus",
		"united economy plus",
		"united polaris economy plus":
		cabin = "P";
		break;
	case "business",
		"united business",
		"united polaris business",
		"businessfirst",
		"united businessfirst":
		cabin = "B";
		break;
	case "global first",
		"united global first",
		"first",
		"united first",
		"united polaris first":
		cabin = "F";
		if isUSADomestic {
			cabin = "B";
		}
		break;
	}
	return cabin;
}

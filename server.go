package main


import (
	"os"
	"net/http"
	"fmt"
	"io/ioutil"
	_ "github.com/joho/godotenv/autoload"
	"bytes"
	"encoding/json"
	"bitbucket.org/ihar76/parsergoplugins/types"
)


type ServerHandler func([]byte)
//var serverHandler ServerHandler



func MainHandlerHTTP(w http.ResponseWriter, r* http.Request) {
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintln(w, "{\"status\":\"ok\",\"answers\":1}")
	body, _ := ioutil.ReadAll(r.Body)
	go Handle(body)
}

//listen socket
func ServeHTTP(){
	fmt.Println("Listen port: " + os.Getenv("PORT"))
	http.HandleFunc("/", MainHandlerHTTP)
	err:=http.ListenAndServe(":" + os.Getenv("PORT"), nil)
	if err!=nil{
		fmt.Println(err)
	}
	fmt.Println("Cancel listen port: " + os.Getenv("PORT"))
}



func SendToHTTP(trips []types.Trip, queryId string) {

	bodyOut := wrapperTrips(trips, queryId)
	resp, err := http.Post(os.Getenv("ANSWER_ENDPOINT"),
		"application/json; charset=UTF-8",
		bytes.NewBuffer(bodyOut))
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Println("post:\n", string(body))
}

func wrapperTrips(trips []types.Trip, queryId string) []byte{
	params := types.OutData{
		os.Getenv("PARSERS_SERVICE_KEY"),
		os.Getenv("PARSER_CODE"),
		types.OutAnswer{queryId, os.Getenv("PARSER_CODE"), trips}	};

	out,_ := json.Marshal(params)
	return out

}
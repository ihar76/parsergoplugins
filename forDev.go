package main

type fromParser struct {
	Key    string `json:"key"`
	Parser string `json:"parser"`
	Answer struct {
		QueryID string `json:"queryId"`
		Parser  string `json:"parser"`
		Trips []struct {
			Parser    string `json:"parser"`
			Direction int    `json:"direction"`
			Seats     int    `json:"seats"`
			Flights []struct {
				Position       int    `json:"position"`
				Parser         string `json:"parser"`
				CarrierCode    string `json:"carrier_code"`
				FlightDuration string `json:"flight_duration"`
				DepartDate     string `json:"depart_date"`
				DepartTime     string `json:"depart_time"`
				DepartCode     string `json:"depart_code"`
				ArriveDate     string `json:"arrive_date"`
				ArriveTime     string `json:"arrive_time"`
				ArriveCode     string `json:"arrive_code"`
				FlightNumber   string `json:"flight_number"`
				Aircraft       string `json:"aircraft"`
				Layover        string `json:"layover"`
				Cabin          string `json:"cabin"`
			} `json:"flights"`
			DepartCode    string        `json:"depart_code"`
			ArriveCode    string        `json:"arrive_code"`
			TripDate      string        `json:"trip_date"`
			Carriers      []string      `json:"carriers"`
			Cabins        []string      `json:"cabins"`
			FlightLegs    []string      `json:"flight_legs"`
			FlightNumbers []string      `json:"flight_numbers"`
			Layovers      []interface{} `json:"layovers"`
			Stops         []interface{} `json:"stops"`
			Miles         int           `json:"miles"`
			Tax           int           `json:"tax"`
			Award         string        `json:"award"`
			TripDuration  string        `json:"trip_duration"`
			Currency      string        `json:"currency"`
			TotalCarrier  string        `json:"total_carrier"`
			TotalCabin    string        `json:"total_cabin"`
		} `json:"trips"`
	} `json:"answer"`
}

type secondPS struct {
	Event string `json:"event"`
	Data struct {
		Key    string `json:"key"`
		Parser string `json:"parser"`
		Answer struct {
			QueryID string `json:"queryId"`
			Parser  string `json:"parser"`
			Trips []struct {
				Parser    string `json:"parser"`
				Direction int    `json:"direction"`
				Seats     int    `json:"seats"`
				Flights []struct {
					Position       int    `json:"position"`
					Parser         string `json:"parser"`
					CarrierCode    string `json:"carrier_code"`
					FlightDuration string `json:"flight_duration"`
					DepartDate     string `json:"depart_date"`
					DepartTime     string `json:"depart_time"`
					DepartCode     string `json:"depart_code"`
					ArriveDate     string `json:"arrive_date"`
					ArriveTime     string `json:"arrive_time"`
					ArriveCode     string `json:"arrive_code"`
					FlightNumber   string `json:"flight_number"`
					Aircraft       string `json:"aircraft"`
					Layover        string `json:"layover"`
					Cabin          string `json:"cabin"`
				} `json:"flights"`
				DepartCode    string        `json:"depart_code"`
				ArriveCode    string        `json:"arrive_code"`
				TripDate      string        `json:"trip_date"`
				Carriers      []string      `json:"carriers"`
				Cabins        []string      `json:"cabins"`
				FlightLegs    []string      `json:"flight_legs"`
				FlightNumbers []string      `json:"flight_numbers"`
				Layovers      []interface{} `json:"layovers"`
				Stops         []interface{} `json:"stops"`
				Miles         int           `json:"miles"`
				Tax           int           `json:"tax"`
				Award         string        `json:"award"`
				TripDuration  string        `json:"trip_duration"`
				Currency      string        `json:"currency"`
				TotalCarrier  string        `json:"total_carrier"`
				TotalCabin    string        `json:"total_cabin"`
			} `json:"trips"`
		} `json:"answer"`
	} `json:"data"`
	Parser     string `json:"parser"`
	ParserType string `json:"parserType"`
}

//from ps
type fromps struct {
	Event string `json:"event"`
	Data struct {
		Parser string `json:"parser"`
		Trips []struct {
			Parser    string `json:"parser"`
			Direction int    `json:"direction"`
			Seats     int    `json:"seats"`
			Flights []struct {
				Position       int    `json:"position"`
				Parser         string `json:"parser"`
				CarrierCode    string `json:"carrier_code"`
				FlightDuration string `json:"flight_duration"`
				DepartDate     string `json:"depart_date"`
				DepartTime     string `json:"depart_time"`
				DepartCode     string `json:"depart_code"`
				ArriveDate     string `json:"arrive_date"`
				ArriveTime     string `json:"arrive_time"`
				ArriveCode     string `json:"arrive_code"`
				FlightNumber   string `json:"flight_number"`
				Aircraft       string `json:"aircraft"`
				Layover        string `json:"layover"`
				Cabin          string `json:"cabin"`
				DepartPlace    string `json:"depart_place"`
				ArrivePlace    string `json:"arrive_place"`
				CarrierName    string `json:"carrier_name"`
			} `json:"flights"`
			DepartCode    string        `json:"depart_code"`
			ArriveCode    string        `json:"arrive_code"`
			TripDate      string        `json:"trip_date"`
			Carriers      []string      `json:"carriers"`
			Cabins        []string      `json:"cabins"`
			FlightLegs    []string      `json:"flight_legs"`
			FlightNumbers []string      `json:"flight_numbers"`
			Layovers      []interface{} `json:"layovers"`
			Stops         []interface{} `json:"stops"`
			Miles         int           `json:"miles"`
			Tax           int           `json:"tax"`
			Award         string        `json:"award"`
			TripDuration  string        `json:"trip_duration"`
			TotalCarrier  string        `json:"total_carrier"`
			TotalCabin    string        `json:"total_cabin"`
			DepartPlace   string        `json:"depart_place"`
			ArrivePlace   string        `json:"arrive_place"`
		} `json:"trips"`
		QueryID string `json:"queryId"`
		APIKey  string `json:"api_key"`
	} `json:"data"`
	QueryID string `json:"queryId"`
	Parser  string `json:"parser"`
}

type packages struct {
	QueryID  string `json:"queryId"`
	Parser   string `json:"parser"`
	Packages []struct {
		Currency string `json:"currency"`
		Miles    string `json:"miles"`
		Tax      string `json:"tax"`
		Trips    []struct {
			DepartCode    string        `json:"depart_code"`
			ArriveCode    string        `json:"arrive_code"`
			Direction     int           `json:"direction"`
			TripDate      string        `json:"trip_date"`
			TripDuration  string        `json:"trip_duration"`
			Stops         []interface{} `json:"stops"`
			Cabins        []string      `json:"cabins"`
			TotalCabin    string        `json:"total_cabin"`
			Carriers      []string      `json:"carriers"`
			TotalCarrier  string        `json:"total_carrier"`
			Layovers      []interface{} `json:"layovers"`
			FlightLegs    []interface{} `json:"flight_legs"`
			FlightNumbers []string      `json:"flight_numbers"`
			Miles         int           `json:"miles"`
			Tax           int           `json:"tax"`
			Award         string        `json:"award"`
			Parser        string        `json:"parser"`
			Flights       []struct {
				Position     int    `json:"position"`
				Parser       string `json:"parser"`
				CarrierCode  string `json:"carrier_code"`
				DepartTime   string `json:"depart_time"`
				DepartDate   string `json:"depart_date"`
				DepartPlace  string `json:"depart_place"`
				ArriveTime   string `json:"arrive_time"`
				ArriveDate   string `json:"arrive_date"`
				ArrivePlace  string `json:"arrive_place"`
				FlightNumber string `json:"flight_number"`
				Layover      string `json:"layover"`
				Aircraft     string `json:"aircraft"`
				Cabin        string `json:"cabin"`
			} `json:"flights"`
			Seats int `json:"seats"`
		} `json:"trips"`
	} `json:"packages"`
}
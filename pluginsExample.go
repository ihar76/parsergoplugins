package main

import (
	"encoding/json"
	"fmt"
	"bitbucket.org/ihar76/parsergoplugins/types"
	"time"
	"path/filepath"
	"plugin"
	"os"
)

type (
	Parser interface {
		GetCode() string
		Run([]byte, types.Proxy) []types.Trip
	}
	Parsers struct {
		elements map[string]Parser
	}
	ChanParsers struct {
		event string
		data  interface{}
		out   chan Parser
	}
)

var (
	t1      time.Time
	param   chan ChanParsers
	parsers Parsers
)

func Handle(body []byte) {
	fmt.Printf("Elapsed time: none")
	InParams := types.Params{}
	json.Unmarshal(body, &InParams)
	t1 = time.Now()
	fmt.Printf("Elapsed time: %v\n", time.Since(t1))
	t1 = time.Now()
	p := make(chan Parser)
	params := ChanParsers{
		"get",
		InParams.Parser,
		p}

	param <- params
	parser := <-p
	trips := []types.Trip{}
	if parser!=nil {
		proxies := types.GetProxies(parser.GetCode())
		fmt.Println(parser.GetCode() + " -- in")
		trips = parser.Run(body, proxies.GetProxy())
	} else {
		fmt.Printf("parser `%s` not found", InParams.Parser)
	}


	fmt.Printf("count trips from %s = %d\n", InParams.Parser, len(trips))
	SendToHTTP(trips, InParams.ID)
}

func main() {
	fmt.Println("load parsers")
	param = make(chan ChanParsers)
	parsers = Parsers{}
	go parsers.listen()


	getPlugins()

	//fmt.Println("start Clear")
	//cycleReloadPlugins()
	//fmt.Println("finish Clear")
	//time.Sleep(1*time.Minute)
	//fmt.Println("start Clear")
	//cycleReloadPlugins()
	//fmt.Println("finish Clear")

	fmt.Println("start server")
	ServeHTTP()
}

func cycleReloadPlugins(){
	for i:=0; i<10000; i++ {
		params := ChanParsers{
			"clearAll",
			"",
			nil}
		param <- params
		getPlugins()
	}
}

func getPlugins() {
	files, _ := filepath.Glob("plugins/*/*.so")
	for _, file := range files {
		plug, err := plugin.Open(file)
		if err != nil {
			fmt.Println(err)
			continue
		}

		symParser, err := plug.Lookup("Parser")
		if err != nil {
			fmt.Println(err)
			continue
		}

		fmt.Println("load file: " + file)
		parser, ok := symParser.(Parser)
		if !ok {
			fmt.Println("unexpected type from module symbol")
			os.Exit(1)
		}

		ch := ChanParsers{
			"add",
			parser,
			nil}

		fmt.Println("add parser: " + file)
		param <- ch
		// 4. use the module
		str := parser.GetCode()

		fmt.Printf("Return `%s` from parser `%s` \n", str, file)

	}
}

// manage list of parsers
func (ps *Parsers) listen() {
	fmt.Println("test ")
	for data := range param {
		switch data.event {
		case "add":
			parser := data.data.(Parser)

			if ps.elements == nil {
				ps.elements = map[string]Parser{}
			}
			ps.elements[parser.GetCode()] = parser
		case "get":
			code := data.data.(string)
			if _,ok:=ps.elements[code]; !ok{
				data.out <- nil
			}else {
				data.out <- ps.elements[code]
			}
		case "clearAll":
			for code, _ := range ps.elements{
				delete(ps.elements, code)
			}
		case "del":
			code := data.data.(string)
			delete(ps.elements, code)

		}
	}
}

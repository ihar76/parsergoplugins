package types

import (
	"os"
	"net/http/cookiejar"
	"encoding/json"
	"math/rand"
	"time"
	"net/http"
	"bytes"
	"io/ioutil"
	"fmt"
	"compress/gzip"
)

func CheckCount(maxTrip int, chFin chan bool) {
	finFlights := 0
	for finFlights < maxTrip {
		<-chFin
		finFlights ++
	}
	close(chFin)
}

func SaveTrip(chTrip chan Trip, trips *[]Trip) {
	for trip := range chTrip {
		if trip.TripDuration=="00:00" {
			tr, _ := json.Marshal(&trip)
			fmt.Println(string(tr))
		}
		*trips = append(*trips, trip)
	}
}

func (s *Slice) GetAccount(parser string, force bool) bool {
	parsed := []Account{}
	if s.Account.ID == 0 || force {
		url := os.Getenv("ACCOUNT_SERVICE_ENDPOINT") + "/account/forParser"
		url += "?key=" + os.Getenv("ACCOUNT_SERVICE_KEY")
		url += "&parserCode=" + parser
		url += "&limit=10"
		jar := cookiejar.Jar{}
		accounts := Get(url, Proxy{}, &jar, map[string]string{})
		if len(accounts) == 0 {
			return false
		}
		err := json.Unmarshal([]byte(accounts), &parsed)
		if err != nil {
			return false
		}

		s.Account = parsed[rand.Intn(len(parsed))]
	}

	return true

}

/**
proxyUrl, err := url.Parse("http://proxyIp:proxyPort")
myClient := &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}}
 */

func MakeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func SendToHTTP(trips []Trip, queryId string) {

	bodyOut := wrapperTrips(trips, queryId)


	//gzip
	//*
	myClient := &http.Client{Transport: &http.Transport{DisableCompression:false}}



	var buf bytes.Buffer
	g := gzip.NewWriter(&buf)
	g.Write(bodyOut)
	g.Close()

	req, _ := http.NewRequest("POST", os.Getenv("ANSWER_ENDPOINT"), bytes.NewBuffer(bodyOut))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Set("Content-Encoding", "gzip")

	resp, _ := myClient.Do(req)
	/*/
	//nogzip
	resp, err := http.Post(os.Getenv("ANSWER_ENDPOINT"),
		"application/json; charset=UTF-8",
		bytes.NewBuffer(bodyOut))
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()
	//*/
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("post:\n", string(body))
}

func SendToHTTPPackages(packages []Package, queryId string) {

	bodyOut := wrapperPackages(packages, queryId)
	resp, err := http.Post(os.Getenv("ANSWER_ENDPOINT"),
		"application/json; charset=UTF-8",
		bytes.NewBuffer(bodyOut))
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Println("post:\n", string(body))
}

func wrapperTrips(trips []Trip, queryId string) []byte {
	params := OutData{
		os.Getenv("PARSERS_SERVICE_KEY"),
		os.Getenv("PARSERS_API_KEY"),
		os.Getenv("PARSER_CODE"),
		OutAnswer{
			queryId,
			os.Getenv("PARSER_CODE"),
			[]Trip{},
			convertTripsToPackages(trips),
		},
	};
	out, _ := json.Marshal(params)
	return out

}

func wrapperPackages(packages []Package, queryId string) []byte {
	params := OutData{
		os.Getenv("PARSERS_SERVICE_KEY"),
		os.Getenv("PARSERS_API_KEY"),
		os.Getenv("PARSER_CODE"),
		OutAnswer{
			queryId,
			os.Getenv("PARSER_CODE"),
			[]Trip{},
			packages,
		},
	};
	out, _ := json.Marshal(params)
	return out

}

func convertTripsToPackages(trips []Trip) []Package {
	packages := []Package{}

	for _, trip := range trips {
		pkg := Package{
			trip.Currency,
			trip.Miles,
			trip.Tax,
			[]Trip{trip},
		}
		packages = append(packages, pkg)
	}
	return packages
}

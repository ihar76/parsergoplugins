package types

import (
	"encoding/json"
	"time"
)

func (d *Date) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}

	dt, _ := time.Parse("2006-01-02", s)

	dtstr2 := dt.Format("Jan 2, 2006")

	*d = Date(dtstr2)
	return nil
}

func (d Date) Formatted(format string) string {

	dt, _ := time.Parse("Jan 2, 2006", string(d))

	dtstr2 := dt.Format(format)

	return dtstr2
}

func (d Date) ToString() string {
	return string(d)
}

package types

import (
	"net/http"
	"os"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"math/rand"
)

type(
	Proxies struct {
		List []Proxy
	}
)

func GetProxies(code string) Proxies{

	//httpClient := &http.Client { Transport: &http.Transport { Proxy:
	//http.ProxyURL(proxyUrl) } }
	//response, err := httpClient.Get("http://golang.org/")

	client := &http.Client{}

	req, _ := http.NewRequest("GET", os.Getenv("PROXY_SERVICE_ENDPOINT") + "proxy/forParser", nil)
	req.Header.Add("Accept", "application/json")

	q := req.URL.Query()
	q.Add("key", os.Getenv("PROXY_SERVICE_KEY"))
	q.Add("parserCode", code)
		q.Add("proxyType", os.Getenv(code + "_PROXY_TYPE"))
	q.Add("limit", os.Getenv("PROXY_LIMIT"))
	req.URL.RawQuery = q.Encode()

	fmt.Println("Proxy url: ", req.URL.String())

	resp, err := client.Do(req)

	ProxiesList := Proxies{}

	if err != nil {
		fmt.Println("proxy error: ", err)
		return ProxiesList
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(body, &ProxiesList.List)
	if err != nil {
		fmt.Println(err)
	}
	return ProxiesList

}

func (pr *Proxies) GetProxy() Proxy{
	if len(pr.List) > 0 {
		return pr.List[rand.Intn(len(pr.List))]
	}
	return Proxy{}
}
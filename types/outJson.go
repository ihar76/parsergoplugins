package types


func (fl *Flight) SetParms(
	Cabin,
	Parser,
	Aircraft string,
	Position int,
	ArriveCode,
	ArriveDate,
	ArriveTime,
	DepartCode,
	DepartDate,
	DepartTime,
	Layover,
	ArrivePlace,
	CarrierCode,
	CarrierName,
	DepartPlace,
	FlightNumber,
	FlightDuration string) error {

	fl.Cabin = Cabin
	fl.Parser = Parser
	fl.Aircraft = Aircraft
	fl.Position = Position
	fl.ArriveCode = ArriveCode
	fl.ArriveDate = ArriveDate
	fl.ArriveTime = ArriveTime
	fl.DepartCode = DepartCode
	fl.DepartDate = DepartDate
	fl.DepartTime = DepartTime
	fl.Layover = Layover
	fl.ArrivePlace = ArrivePlace
	fl.CarrierCode = CarrierCode
	fl.CarrierName = CarrierName
	fl.DepartPlace = DepartPlace
	fl.FlightNumber = FlightNumber
	fl.FlightDuration = FlightDuration

	return nil
}


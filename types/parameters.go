package types

import "net/http/cookiejar"

type (
	ServerHandler func([]byte)
	Date string

	Award struct {
		Type  string  `json:"type"`
		Miles float64 `json:"miles"`
		Tax   string  `json:"tax"`
	}

	Slice struct {
		Passengers int    `json:"passengers"`
		From       string `json:"from"`
		To         string `json:"to"`
		Dates      []Date `json:"dates"`
		Direction  int    `json:"direction"`
		Proxy      Proxy  `json:"proxy"`
		Jars       map[string]cookiejar.Jar
		Account    Account
	}

	Params struct {
		ID           string   `json:"id"`
		UUID         string   `json:"uuid"`
		Parser       string   `json:"parser"`
		Type         string   `json:"type"`
		ProxyService string   `json:"proxyService"`
		ProxyType    string   `json:"proxyType"`
		Passengers   int      `json:"passengers"`
		Slices       []Slice  `json:"slices"`
		Cabins       []string `json:"cabins"`
	}

	Trip struct {
		Parser        string   `json:"parser"`
		Direction     int      `json:"direction"`
		Seats         int      `json:"seats"`
		Flights       []Flight `json:"flights"`
		DepartCode    string   `json:"depart_code"`
		DepartPlace   string   `json:"depart_place"`
		ArriveCode    string   `json:"arrive_code"`
		ArrivePlace   string   `json:"arrive_place"`
		TripDate      string   `json:"trip_date"`
		Carriers      []string `json:"carriers"`
		Cabins        []string `json:"cabins"`
		FlightLegs    []string `json:"flight_legs"`
		FlightNumbers []string `json:"flight_numbers"`
		Layovers      []string `json:"layovers"`
		Stops         []string `json:"stops"`
		Miles         float64  `json:"miles"`
		Tax           string   `json:"tax"`
		Award         string   `json:"award"`
		TripDuration  string   `json:"trip_duration"`
		TotalCarrier  string   `json:"total_carrier"`
		TotalCabin    string   `json:"total_cabin"`
		Currency      string   `json:"currency"`
	}

	Flight struct {
		Cabin          string `json:"cabin"`
		Parser         string `json:"parser"`
		Aircraft       string `json:"aircraft"`
		Position       int    `json:"position"`
		ArriveCode     string `json:"arrive_code"`
		ArriveDate     string `json:"arrive_date"`
		ArriveTime     string `json:"arrive_time"`
		DepartCode     string `json:"depart_code"`
		DepartDate     string `json:"depart_date"`
		DepartTime     string `json:"depart_time"`
		Layover        string `json:"layover"`
		ArrivePlace    string `json:"arrive_place"`
		CarrierCode    string `json:"carrier_code"`
		CarrierName    string `json:"carrier_name"`
		DepartPlace    string `json:"depart_place"`
		FlightNumber   string `json:"flight_number"`
		FlightDuration string `json:"flight_duration"`
	}

	Package struct {
		Currency string  `json:"currency"`
		Miles    float64 `json:"miles"`
		Tax      string  `json:"tax"`
		Trips    []Trip  `json:"trips"`
	}

	OutAnswer struct {
		QueryID  string    `json:"queryId"`
		Parser   string    `json:"parser"`
		Trips    []Trip    `json:"trips"`
		Packages []Package `json:"packages"`
	}

	OutData struct {
		ApiKey string    `json:"api_key"`
		Key    string    `json:"key"`
		Parser string    `json:"parser"`
		Answer OutAnswer `json:"answer"`
	}

	Price struct {
		Cabin string `json:"cabins"`
		Award Award  `json:"award"`
	}

	Proxy struct {
		Broken          map[string]bool `json:"broken"`
		CreatedAt       string          `json:"created_at"`
		CustomCommandAt string          `json:"custom_command_at"`
		DcID            string          `json:"dcId"`
		ID              string          `json:"id"`
		IP              string          `json:"ip"`
		KyUsageTime     string          `json:"ky_usage_time"`
		OsID            string          `json:"osId"`
		PlanID          string          `json:"planId"`
		Port            int             `json:"port"`
		Provider        string          `json:"provider"`
		ProxyAuth       int             `json:"proxy_auth"`
		ProxyLogin      string          `json:"proxy_login"`
		ProxyPassword   string          `json:"proxy_password"`
		ProxyType       string          `json:"proxy_type"`
		RootPassword    string          `json:"root_password"`
		ServiceID       string          `json:"serviceId"`
		UpdatedAt       string          `json:"updated_at"`
	}

	Account struct {
		ID         int         `json:"id"`
		Name       string      `json:"name"`
		Login      string      `json:"login"`
		Password   string      `json:"password"`
		Pin        string      `json:"pin"`
		Used       int         `json:"used"`
		Active     int         `json:"active"`
		Reserved   int         `json:"reserved"`
		CreatedAt  string      `json:"created_at"`
		UpdatedAt  string      `json:"updated_at"`
		LastUsedAt string      `json:"last_used_at"`
		BrokenAt   interface{} `json:"broken_at"`
	}
)

package types

import (
	"os"
	"net/http"
	"fmt"
	"io/ioutil"
)

var serverHandler ServerHandler



func MainHandlerHTTP(w http.ResponseWriter, r* http.Request) {
	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintln(w, "{\"status\":\"ok\",\"answers\":1}")
	body, _ := ioutil.ReadAll(r.Body)
	go serverHandler(body)
}

func ServeHTTP(MainHandler ServerHandler){
	serverHandler = MainHandler
	fmt.Println("Listen port: " + os.Getenv("PORT"))
	http.HandleFunc("/", MainHandlerHTTP)
	http.ListenAndServe(":" + os.Getenv("PORT"), nil)
}
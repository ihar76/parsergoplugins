package types


import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"net/url"
	"strconv"
	"net/http/cookiejar"
	"strings"
	"reflect"
)

func Get(urls string, proxy Proxy, jar *cookiejar.Jar, headers map[string]string) string {

	var myClient *http.Client

	if true && proxy.ProxyLogin != "" {
		proxyUrl, _ := url.Parse("http://" + proxy.ProxyLogin + ":" + proxy.ProxyPassword + "@"+ proxy.IP + ":" + strconv.Itoa(proxy.Port))
		//proxyUrl, _ := url.Parse("http://127.0.0.1:8889")
		myClient = &http.Client{Transport: &http.Transport{Proxy: http.ProxyURL(proxyUrl)}, Jar: jar}
	} else {
		myClient = &http.Client{Transport: &http.Transport{}, Jar: jar}
	}

	var jsonStr = []byte{}

	req, err := http.NewRequest("GET", urls, bytes.NewBuffer(jsonStr))

	if headers!=nil{
		for key, val := range headers{
			req.Header.Set(key, val)
		}
	}



	response, err := myClient.Do(req)
	if err != nil {
		fmt.Printf("%s", err)
		os.Exit(1)
	} else {
		defer response.Body.Close()

		var contents []byte
		if strings.Contains(response.Header.Get("Content-Encoding"), "gzip") {
			reader, _ := gzip.NewReader(response.Body)
			contents, _ = ioutil.ReadAll(reader)

		} else {
			contents, _ = ioutil.ReadAll(response.Body)
		}

		return string(contents)
	}
	return ""
}

func PostJSON(urlto, jsonData string, headers map[string]string, proxy Proxy, jar *cookiejar.Jar) (string, *http.Response ) {
	var jsonStr = []byte(jsonData)
	var resp *http.Response
	req, err := http.NewRequest("POST", urlto, bytes.NewBuffer(jsonStr))
	//req.Header.Set("Content-Type", "application/json")
	for key, val := range headers {
		req.Header.Set(key, val)
	}
	// for basic auth
	/*
	auth := proxy.ProxyLogin + ":" + proxy.ProxyPassword
	basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))
	req.Header.Add("Proxy-Authorization", basicAuth)
	*/
	if proxy.IP != "" {
		fmt.Println("start post with proxy: ", urlto)
		proxyString := "http://" + proxy.ProxyLogin + ":" + proxy.ProxyPassword + "@" + proxy.IP + ":" + strconv.Itoa(proxy.Port)
		//proxyString := "http://127.0.0.1:8889"
		//fmt.Println("start post with proxy: " + proxyString)
		proxyUrl, err := url.Parse(proxyString)
		if err != nil {
			fmt.Println("Error proxy data")
		}
		httpClient := &http.Client{Transport: &http.Transport{Proxy:
		http.ProxyURL(proxyUrl)}, Jar: jar}

		resp, err = httpClient.Do(req)
		if err != nil {
			fmt.Println(err)
		}
	}else {
		fmt.Println("start post without proxy: ", urlto)
		client := &http.Client{Jar: jar}
		resp, err = client.Do(req)
		if err != nil {
			fmt.Println(err)
		}
	}

	defer resp.Body.Close()

	body := []byte{}
	switch resp.Header.Get("Content-Encoding") {
	case "gzip":
		gz, err := gzip.NewReader(resp.Body)
		if err != nil {

		}
		defer gz.Close()

		buff := make([]byte, 1024)
		for {
			n, err := gz.Read(buff)

			if err != nil && err != io.EOF {
				panic(err)
			}

			if n == 0 {
				break
			}
			body = append(body, buff[:n]...)
		}

	default:
		// just use the default reader
		body, _ = ioutil.ReadAll(resp.Body)
	}

	// fmt.Println("response Status:", resp.Status)
	// fmt.Println("response Headers:", resp.Header)

	return string(body), resp
}

func PostForm(urlto string, formData url.Values, headers map[string]string, proxy Proxy, jar *cookiejar.Jar) (string, *http.Response ){


	var resp *http.Response
	fmt.Println(formData.Encode())
	req, err := http.NewRequest("POST", urlto, strings.NewReader(formData.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	for key, val := range headers {
		req.Header.Set(key, val)
	}
	//req.PostForm = formData
	// for basic auth
	/*
	auth := proxy.ProxyLogin + ":" + proxy.ProxyPassword
	basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))
	req.Header.Add("Proxy-Authorization", basicAuth)
	*/

	if proxy.IP != ""{
		fmt.Println("start post with proxy: ", urlto)
		proxyString := "http://" + proxy.ProxyLogin + ":" + proxy.ProxyPassword + "@" + proxy.IP + ":" + strconv.Itoa(proxy.Port)
		//proxyString := "http://127.0.0.1:8889"
		//fmt.Println("start post with proxy: " + proxyString)
		proxyUrl, err := url.Parse(proxyString)
		if err != nil {
			fmt.Println("Error proxy data")
		}
		httpClient := &http.Client{Transport: &http.Transport{Proxy:
		http.ProxyURL(proxyUrl)}, Jar: jar}

		resp, err = httpClient.Do(req)
		if err != nil {
			fmt.Println(err)
		}
	}else {
		fmt.Println("start post without proxy: ", urlto)
		client := &http.Client{Jar: jar}
		resp, err = client.Do(req)
		if err != nil {
			fmt.Println(err)
		}
	}

	defer resp.Body.Close()

	body := []byte{}
	switch resp.Header.Get("Content-Encoding") {
	case "gzip":
		gz, err := gzip.NewReader(resp.Body)
		if err != nil {

		}
		defer gz.Close()

		buff := make([]byte, 1024)
		for {
			n, err := gz.Read(buff)

			if err != nil && err != io.EOF {
				fmt.Println(err)
				break
			}

			if n == 0 {
				break
			}
			body = append(body, buff[:n]...)
		}

	default:
		// just use the default reader
		body, _ = ioutil.ReadAll(resp.Body)
	}

	// fmt.Println("response Status:", resp.Status)
	// fmt.Println("response Headers:", resp.Header)

	return string(body), resp
}

func StructToMap(i interface{}) (values url.Values) {
	values = url.Values{}
	iVal := reflect.ValueOf(i).Elem()
	typ := iVal.Type()
	for i := 0; i < iVal.NumField(); i++ {
		f := iVal.Field(i)
		// You ca use tags here...
		// tag := typ.Field(i).Tag.Get("tagname")
		// Convert each type into a string for the url.Values string map
		var v string
		switch f.Interface().(type) {
		case int, int8, int16, int32, int64:
			v = strconv.FormatInt(f.Int(), 10)
		case uint, uint8, uint16, uint32, uint64:
			v = strconv.FormatUint(f.Uint(), 10)
		case float32:
			v = strconv.FormatFloat(f.Float(), 'f', 4, 32)
		case float64:
			v = strconv.FormatFloat(f.Float(), 'f', 4, 64)
		case []byte:
			v = string(f.Bytes())
		case string:
			v = f.String()
		}
		r := typ.Field(i).Tag.Get("json")
		values.Set(r, v)
		//r := typ.Field(i)
		//values.Set(r.Name, v)
	}
	return
}
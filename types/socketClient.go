package types


import (
	"net"
	"log"
	"os"
)

const (
	StopCharacter = "\r\n\r\n"
)

func SendToSocket(message string) {

	address := os.Getenv("ANSWER_ENDPOINT")
	conn, err := net.Dial("tcp", address)

	defer conn.Close()

	if err != nil {
		log.Fatalln(err)
	}

	conn.Write([]byte(message))
	conn.Write([]byte(StopCharacter))
	log.Printf("Send: %s", message)

	buff := make([]byte, 1024)
	n, _ := conn.Read(buff)
	log.Printf("Receive: %s", buff[:n])

}


package types

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"os"
	"log"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"os/exec"
)

func SendSlaves(trips []Trip, code, id, query string) {

	db, err := sql.Open(
		"mysql",
		os.Getenv("DB_USERNAME")+
			":"+ os.Getenv("DB_PASSWORD")+
			"@"+
			"/"+ os.Getenv("DB_DATABASE"))

	if err != nil {
		log.Println("Error connect to DB", err) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	sql := `SELECT pd.slave, pd.tax_coefficient FROM parsers 
	LEFT JOIN parser_dependencies AS pd ON parsers.code = pd.master
	WHERE parsers.enabled_master=1 AND pd.master=?;`

	stmtOut, err := db.Query(sql, code)
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	pathProject := os.Getenv("PATH_PROJECT")
	filename := id + ":" + code + ".json";
	fullPath := pathProject + "/storage/answers/" + filename
	jsonTrips, _ := json.Marshal(trips)
	err = ioutil.WriteFile(fullPath, jsonTrips, 0644)

	if err != nil {
		log.Println(err)
	}

	for stmtOut.Next() {
		var slave string
		var tax_coefficient float64
		err = stmtOut.Scan(&slave, &tax_coefficient)
		fmt.Println(slave, tax_coefficient)
		cmd := "php"
		tc := strconv.FormatFloat(tax_coefficient, 'f', 4, 64)
		cmdRun := exec.Command(cmd,
			os.Getenv("PATH_PROJECT")+"/artisan",
			slave+":slave",
			query,
			code,
			tc,
		)

		go fmt.Println(cmdRun.Run())
	}

}
